# Prova de Conceito: Smart Clothing 
**Cenário:**

Demosntrar agilidade e aderencia na estratégia de desenvolvimento do APP para campanha de 100 anos de forma paralelizada, segura, aderente a arquiettura e evoutiva


* **PreRequisitos**  
    *  Start: AnyPoint Studio
    *  Start: ProjectDemo
    *  Start: Postman
    *  Start: SOAP-UI


**API First**

**Cenário:**

Para ganhar velocidade no desenvolvimento, foram divididas em  etapas paralelas a construção da plataforma digital
sendo:
*  Criar o aplicativo mobile
*  Criar a API que consulta dados no webservice 


**MockingService**

* Gerado o contrato de "serviço" através do RAML na plataforma Desing Center (Low Code)
* Enviado aos times da empresa espacialista no mobile


**https://anypoint.mulesoft.com/**
* use: umbertoss04
* pwd: Meta1604ms*



**inputOrder**: /ordes
```
#%RAML 1.0 DataType
type: object
properties:
    size:
        type: string
    email:
        type: string
    name: 
       type: string
    adress1: 
       type: string
    adress2: 
       type: string
    city: 
       type: string
    stateOrProvince: 
       type: string
    postalCode: 
       type: string
    country: 
       type: string
```
**outputOrder**
```
#%RAML 1.0 DataType
type: object
properties:
    orderId:
        type: string
 ```       
**inputOrderTrackOrder**: /trackorder   
```
#%RAML 1.0 DataType
type: object
properties:
    email:
        type: string
    orderId:
        type: string
```        
**outputOrderTrackOrder**   
```
#%RAML 1.0 DataType
type: object
properties:
    orderId:
        type: string
    status:
       type: string
    size:
       type: string
```

**API LED**
**Cenário:**
*  API Dividida em três camadas 
    * Camada de experiencia enviada via RAML e MockingService ao time de mobile 
    * Camada de processo: efetuada a transforamção XML to Json
    * Camada de dados: gerado o consumer do endpoint: http://tshirts.demos.mulesoft.com/?wsdl 

**Objetivo:** Atender ao time-to-marketing da camapnha de comemorativa de 100 anos paralelizando o desenvolvimento, garantindo resuo e arquitetua enterprise

# Consumo
**Postman**

* TshirtService: http://localhost:8081/inventory


##

* order_tshirt_mulesoft:  http://Localhost:8084/ordes

**POST_Request**
```
{
    "size": "M",
    "email": "catae@gmail.com",
    "name": "catae",
    "adress1": "rua um",
    "adress2": "rua 2",
    "city": "BH",
    "stateOrProvince": "MG",
    "postalCode": "30150640",
    "country": "Brasil"
}
```
**POST_Response**
```
{
    "orderId": "1011"
}

```
##
* trackorder:  http://Localhost:8083/trackorder

**POST_Request**
```
{
    "email": "catae@gmail.com",
    "orderId": "1011"
    
}

```
**POST_Response**
```
{
    "orderId": "1011",
    "status": "CREATED",
    "size": "M"
}
```
**SOAP-UI**

**OrdesTShirts**

````
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tsh="http://mulesoft.org/tshirt-service">
   <soapenv:Header/>
   <soapenv:Body>
      <tsh:OrderTshirt>
         <size>M</size>
         <email>msilva@gmail.com</email>
         <name>Umberto</name>
         <address1>rua don lucio antunes 424</address1>
         <address2>rua gentil teodoro 455</address2>
         <city>BH</city>
         <stateOrProvince>MG</stateOrProvince>
         <postalCode>30150640</postalCode>
         <country>?</country>
      </tsh:OrderTshirt>
   </soapenv:Body>
</soapenv:Envelope>
````

**TrackOrder**
````
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tsh="http://mulesoft.org/tshirt-service">
   <soapenv:Header/>
   <soapenv:Body>
      <tsh:TrackOrder>
         <email>catatau@gmail.com</email>
         <orderId>1007</orderId>
      </tsh:TrackOrder>
   </soapenv:Body>
</soapenv:Envelope>
````



##




 
 


